      .data
      input_msg: .asciiz "input number:"
      output_msg: .asciiz "factorial of input number is:"
      err_msg: .asciiz " input is negative. try again.\n"


      .text
      .globl main

main: 
      li $v0, 4
      la $a0, input_msg
      syscall
      li $v0, 5
      syscall
      move $t0, $v0
      bgez $t0,next
      li $v0, 4
      la $a0, err_msg
      syscall
      j main

next:    
      addi $sp, $sp, -4           # save $ra 
      sw $ra, 4($sp)              # save $ra in allocated stack space 
      move $a0, $t0
      jal Factorial
      move $t0, $v0
      li $v0, 4
      la $a0, output_msg
      syscall
      li $v0, 1
      move $a0, $t0
      syscall
      lw $ra, 4($sp)
      addi $sp, $sp, 4
      jr $ra                      # return from main


Factorial:

      addi $sp, $sp, -4           # save $ra 
      sw $ra, 4($sp)              # save $ra in allocated stack space
      beqz $a0, terminate
      addi $sp, $sp, -4           # save $ra 
      sw $a0, 4($sp)              # save $ra in allocated stack space
      sub $a0, $a0, 1
      jal Factorial
      lw $t0, 4($sp)
      mul $v0, $v0, $t0
      lw $ra, 8($sp)
      addi $sp, $sp, 8
      jr $ra                      # return from main


terminate:
         
      li $v0, 1                   # restore $ra form stack
      lw $ra,4($sp)
      addi $sp, $sp, 4
      jr $ra                      # return from main 

                