      .text
      .globl main

main: 
      add $sp, $sp, -4            # substract 4 form stack pointer
      sw $ra, 4($sp)              # save $ra in allocated stack space 
      jal test                    
      nop                         
      lw $ra, 4($sp)              # restore $ra form stack
      addi $sp, $sp, 4            # free stack space
      jr $ra                      # return from main
   
test: 
      nop                         # this is the procedure named ‘test’
      jr $ra                      # return from this procedure